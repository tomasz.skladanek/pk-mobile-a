package pl.wat.dogweb.adapters;

import android.content.ClipData;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Locale;

import pl.wat.dogweb.DogListActivity;
import pl.wat.dogweb.HomeScreen;
import pl.wat.dogweb.MainActivity;
import pl.wat.dogweb.R;
import pl.wat.dogweb.models.Dog;

public class DogListAdapter extends RecyclerView.Adapter<DogListAdapter.ViewHolder> {

    private final List<Dog> dogs;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener (OnItemClickListener listener){
        mListener = listener;
    }

    public DogListAdapter(List<Dog> dogs) {
        this.dogs = dogs;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.dog_element, parent, false);

        ViewHolder viewHolder = new ViewHolder(contactView, mListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Dog dog = dogs.get(position);

        TextView textViewName = holder.textView_name;
        textViewName.setText(dog.getName());
        TextView textViewRace = holder.textView_race;
        textViewRace.setText(dog.getRace().toString().replace('_',' ').toLowerCase(Locale.ROOT));
        TextView textViewDate = holder.textView_date;
        textViewDate.setText(dog.getAddedDate());
    }


    @Override
    public int getItemCount() {
        if (dogs == null) {
            Log.e("ADAPTER", "DOG LIST MAY BE NULL OR EMPTY");
            return 0;
        } else {
            return dogs.size();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textView_name;
        public TextView textView_race;
        public TextView textView_date;


        public ViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);

            textView_name = (TextView) itemView.findViewById(R.id.dog_name);
            textView_race = (TextView) itemView.findViewById(R.id.dog_race);
            textView_date = (TextView) itemView.findViewById(R.id.dog_date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener != null){
                        int position = getAdapterPosition();
                            if(position != RecyclerView.NO_POSITION){
                                listener.onItemClick(position);
                            }
                    }
                }
            });
        }
    }
}
