package pl.wat.dogweb.service;

import java.util.List;

import pl.wat.dogweb.models.AddDogRequest;
import pl.wat.dogweb.models.Dog;
import pl.wat.dogweb.models.GetDogByIdRequest;
import pl.wat.dogweb.models.LoginRequest;
import pl.wat.dogweb.models.LoginResponse;
import pl.wat.dogweb.models.RegisterUserRequest;
import pl.wat.dogweb.models.StringResponse;
import pl.wat.dogweb.models.StringResponse;
import pl.wat.dogweb.models.UpdateAddressRequest;
import pl.wat.dogweb.models.UpdateContactRequest;
import pl.wat.dogweb.models.UpdateDescriptionRequest;
import pl.wat.dogweb.models.UpdateRoleRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface RetrofitInterface {



    @GET("dogs/random")
    Call<Dog> getRandomDog(@Header("Authorization") String token);

    @GET("dogs/all")
    Call<List<Dog>> getAllDogs(@Header("Authorization") String token);

    @GET("dogs/get-Dog")
    Call<Dog> getDog();

    @POST("dogs/add-Dog")
    Call<StringResponse> addDog(@Body AddDogRequest addDogRequest, @Header("Authorization") String token);

    @POST("login")
    Call<LoginResponse> requestAuth(@Body LoginRequest loginRequest);

    @POST("user/add")
    Call<StringResponse> requestRegistration(@Body RegisterUserRequest registerUserRequest);

    @PUT("user/role")
    Call<StringResponse> requestUpdateRole(@Header("Authorization") String token, @Body UpdateRoleRequest updateRoleRequest);

    @PUT("shelter/updateaddress")
    Call<StringResponse> requestUpdateAddress(@Header("Authorization") String token,@Body UpdateAddressRequest updateAddressRequest);

    @PUT("shelter/updatecontact")
    Call<StringResponse> updateContactRequest(@Header("Authorization") String token, @Body UpdateContactRequest updateContactRequest);

    @PUT("shelter/update")
    Call<StringResponse> updateDescriptionRequest(@Header("Authorization") String token, @Body UpdateDescriptionRequest updateDescriptionRequest);
}