package pl.wat.dogweb;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;


import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;


import pl.wat.dogweb.models.SharedPrefsDefaults;
import pl.wat.dogweb.models.StringResponse;
import pl.wat.dogweb.models.UpdateAddressRequest;
import pl.wat.dogweb.models.UpdateContactRequest;
import pl.wat.dogweb.service.RetrofitClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ShelterContactScreen extends AppCompatActivity {

    AtomicBoolean contactSuccess = new AtomicBoolean(false);


    public static final String TOKEN = SharedPrefsDefaults.TOKEN;

    private SharedPreferences sharedPreferences;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_shelter_contact);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

    }

    public void UpdateContact(View view){

        EditText email_check = findViewById(R.id.shelter_contact_email);
        EditText wwwPage_check = findViewById(R.id.shelter_contact_wwwPage);
        EditText phone_check = findViewById(R.id.shelter_contact_phone);

        if(checkIfEmptyContact(email_check,wwwPage_check,phone_check)){
            Long shelterId= 1L;
            String email_txt = email_check.getText().toString();
            String wwwPage_txt = wwwPage_check.getText().toString();
            String phone_txt = phone_check.getText().toString();
            // ???????????????????????????????????????????????????????????????????????????????????????????????????????????????????

            UpdateContactRequest updateContactRequest= new UpdateContactRequest(shelterId,email_txt,wwwPage_txt,phone_txt);

            new RetrofitClass().getService().updateContactRequest(PreferenceManager.getDefaultSharedPreferences(this).getString(SharedPrefsDefaults.TOKEN, null),updateContactRequest).enqueue(new Callback<StringResponse>() {
                @Override
                public void onResponse(Call<StringResponse> call, Response<StringResponse> response) {
                    if(response.body().getResp().equals("????????????????????????????????????????????????????"))
                        Toast.makeText(ShelterContactScreen.this, response.body().getResp(), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(Call<StringResponse> call, Throwable t) {
                    Toast.makeText(ShelterContactScreen.this, "Error", Toast.LENGTH_LONG).show();
                }
            });

        }
    }


    // Funkcja sprawdzająca czy został wpisany email, strona www i telefon do schroniska
    public boolean checkIfEmptyContact(EditText email_check,
                                       EditText wwwPage_check,
                                       EditText phone_check
                                       ) {

        boolean corr1, corr2,corr3;

        // Sprawdź czy puste
        boolean email = email_check.getText().toString().isEmpty();
        boolean wwwPage = wwwPage_check.getText().toString().isEmpty();
        boolean phone = phone_check.getText().toString().isEmpty();


        TextInputLayout email_check_parent = findViewById(R.id.shelter_contact_email_parent);
        TextInputLayout wwwPage_check_parent = findViewById(R.id.shelter_contact_wwwPage_parent);
        TextInputLayout phone_check_parent =findViewById(R.id.shelter_contact_phone_parent);


        // Weryfikacja emailu
        if (email) {
            email_check_parent.setError(getString(R.string.remail_cant_be_empty));
            corr1 = false;
        } else corr1 = true;

        // Weryfikacja strony www
        if (wwwPage) {
            wwwPage_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr2 = false;
        } else corr2 = true;

        // Weryfikacja telefonu
        if (phone) {
            phone_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr3 = false;
        } else corr3 = true;


        if (corr1 && corr2 && corr3) {
            contactSuccess.set(true);
        } else contactSuccess.set(false);

        return contactSuccess.get();
    }
}