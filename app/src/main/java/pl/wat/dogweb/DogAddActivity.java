package pl.wat.dogweb;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import pl.wat.dogweb.models.AddDogRequest;
import pl.wat.dogweb.models.AlertBox;
import pl.wat.dogweb.models.DogRace;
import pl.wat.dogweb.models.SharedPrefsDefaults;
import pl.wat.dogweb.models.StringResponse;
import pl.wat.dogweb.service.NetworkCheck;
import pl.wat.dogweb.service.RetrofitClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DogAddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dog_add);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        DogRace[] values = DogRace.values();
        ArrayList<String> valuesAsString = new ArrayList<>();
        for (DogRace dr : values) {
            valuesAsString.add(dr.toString().replace('_',' ').toLowerCase(Locale.ROOT));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, valuesAsString);
        AutoCompleteTextView textView = findViewById(R.id.dog_add_race);
        textView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void dogSend(View view) {

        // FIXME: 10.08.2021

        if (new NetworkCheck(getApplicationContext()).isNetworkAvailable()) {

            TextInputLayout dog_name = findViewById(R.id.dog_add_name);
            String name = Objects.requireNonNull(dog_name.getEditText()).getText().toString();

            TextInputLayout obj_shelterId = findViewById(R.id.dog_add_shelterid);
            String string_shelterId = Objects.requireNonNull(obj_shelterId.getEditText()).getText().toString();
            int shelterId = Integer.parseInt(string_shelterId);

            TextInputLayout obj_dogId = findViewById(R.id.dog_add_id);
            String dogId = Objects.requireNonNull(obj_dogId.getEditText()).getText().toString();

            AutoCompleteTextView dog_race = findViewById(R.id.dog_add_race);
            String race = dog_race.getText().toString().replace(' ', '_').toUpperCase(Locale.ROOT);

            // TODO zmienić userID na faktyczne ID usera.
            AddDogRequest addDogRequest = new AddDogRequest(false,"DO UZUPEŁNIENIA", dogId, 2,"DO UZUPEŁNIENIA", name, race, shelterId);

            new RetrofitClass().getService().addDog(addDogRequest, PreferenceManager.getDefaultSharedPreferences(this).getString(SharedPrefsDefaults.TOKEN,null)).enqueue(new Callback<StringResponse>() {
                @Override
                public void onResponse(Call<StringResponse> call, Response<StringResponse> response) {

                    if (response.body() != null) {
                        switch (response.code()) {
                            case 403:
                                Toast.makeText(DogAddActivity.this, "Błąd uwierzytelniania", Toast.LENGTH_SHORT).show();
                            case 200:
                                Toast.makeText(DogAddActivity.this, "Dodano psa!", Toast.LENGTH_SHORT).show();
                            default:
                                Toast.makeText(DogAddActivity.this, response.code(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<StringResponse> call, Throwable t) {
                    Toast.makeText(DogAddActivity.this, "Błąd komunikacji z serwerem!", Toast.LENGTH_SHORT).show();
                    Log.i("dog-Add/onFailure", t.toString(), t);
                }
            });

            finish();


            //TODO Tutaj kod dodający psa do bazy danych.

        } else {
            new AlertBox(DogAddActivity.this, R.string.no_connection_add_dog).noWifi();
        }
    }
}