package pl.wat.dogweb.models;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;

import pl.wat.dogweb.R;

public class AlertBox {

    Context context;
    int stringID;

    public AlertBox(Context context, int stringID) {
        this.context = context;
        this.stringID = stringID;
    }

    public void noWifi() {
        new AlertDialog
                .Builder(context)
                .setTitle(R.string.no_connection)
                .setMessage(stringID)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                })
                .setIcon(R.drawable.ic_baseline_wifi_off_24)
                .show();
    }

}
