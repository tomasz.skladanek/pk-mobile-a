package pl.wat.dogweb.models;
import pl.wat.dogweb.models.Dog;

public class Singletone {

    static User currentUser;
    static String jwt;

    static Dog selectedDog;

    static Dog description;

    public Singletone() {
    }


    public static Dog getSelectedDog() {
        return selectedDog;
    }

    public static void setSelectedDog(Dog selectedDog) {
        Singletone.selectedDog = selectedDog;
    }

    public static User getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(User currentUser) {
        Singletone.currentUser = currentUser;
    }

    public static String getJwt() {
        return jwt;
    }

    public static void setJwt(String jwt) {
        Singletone.jwt = jwt;
    }
}
