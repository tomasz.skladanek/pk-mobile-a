package pl.wat.dogweb.models;

public class RegisterUserRequest {

    String email;
    String password;
    Long shelterId;

    public RegisterUserRequest() {

    }

    public RegisterUserRequest(String email, String password,Long shelterId) {
        this.email = email;
        this.password = password;
        this.shelterId= shelterId;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getShelterId() {
        return shelterId;
    }

    public void setShelterId(Long shelterId) {
        this.shelterId = shelterId;
    }

}
