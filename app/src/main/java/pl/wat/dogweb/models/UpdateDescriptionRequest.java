package pl.wat.dogweb.models;

public class UpdateDescriptionRequest {

    Long id;
    String name;
    String description;
    String nip;

    public UpdateDescriptionRequest(){

    }

    public UpdateDescriptionRequest(Long id, String name, String description, String nip){
        this.id=id;
        this.name=name;
        this.description=description;
        this.nip=nip;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
