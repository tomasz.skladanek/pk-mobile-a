package pl.wat.dogweb.models;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

public class SharedPrefsDefaults {

    // Uwierzytelnianie
    public static final String GUEST_MODE = "Guest mode";

    public static final String TOKEN = "Authorization: ";

    public static final String LOGIN = "Login";
    public static final String PASSWORD = "Password";

    public static final String REMEMBER_ME = "Remember me";

    // Ustawienia użytkownika



    public SharedPrefsDefaults() {

    }

    public SharedPreferences getSharedPrefs(Context context) {return PreferenceManager.getDefaultSharedPreferences(context);}

    public String getGuestMode() {return GUEST_MODE;}

    public String getLogin() {return LOGIN;}

    public String getPassword() {return PASSWORD;}

    public String getRememberMe() {return REMEMBER_ME;}

    public static String getTOKEN() {
        return TOKEN;
    }
}
