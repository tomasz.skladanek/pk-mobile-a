package pl.wat.dogweb.models;

public class UpdateAddressRequest {
    Long id;
    String city;
    String street;
    String buildingNumber;
    String postalCode;
    String province;

    public UpdateAddressRequest(){

    }


    public UpdateAddressRequest(Long id, String city, String street, String buildingNumber, String postalCode, String province){
        this.id=id;
        this.city=city;
        this.street=street;
        this.buildingNumber=buildingNumber;
        this.postalCode=postalCode;
        this.province=province;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
