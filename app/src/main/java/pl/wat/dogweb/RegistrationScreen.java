package pl.wat.dogweb;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import pl.wat.dogweb.models.RegisterUserRequest;
import pl.wat.dogweb.models.StringResponse;
import pl.wat.dogweb.service.RetrofitClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationScreen extends AppCompatActivity {

    AtomicBoolean registerSuccess = new AtomicBoolean(false);



    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_screen);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

    }


    public void Registration(View view){

        EditText email_check = findViewById(R.id.remail_here);
        EditText password_check = findViewById(R.id.rpassword_here);
        EditText password_confirmation_check = findViewById(R.id.rpassword_conf_here);


            Long shelterId= 1L;
            String email_txt = email_check.getText().toString();
            String password_txt = password_check.getText().toString();

            RegisterUserRequest registerUserRequest= new RegisterUserRequest(email_txt,password_txt,shelterId);

                new RetrofitClass().getService().requestRegistration(registerUserRequest).enqueue(new Callback<StringResponse>(){
                    @Override
                    public void onResponse(Call<StringResponse> call, Response<StringResponse> response) {
                        if (response.body().getResp().equals("Udało się utworzyć użytkownika")) {
                            startActivity(new Intent(RegistrationScreen.this, LoginScreen.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
                            Toast.makeText(RegistrationScreen.this, response.body().getResp(), Toast.LENGTH_LONG).show();
                        }
                        else if (response.body().getResp().equals("Użytkownik o podanym adresie e-mail="+email_txt+" już istnieje")){
                            Toast.makeText(RegistrationScreen.this, response.body().getResp(), Toast.LENGTH_LONG).show();
                        }
                        else if(response.body().getResp().equals("Podane schronisko nie istnieje")){
                            Toast.makeText(RegistrationScreen.this, response.body().getResp(), Toast.LENGTH_LONG).show();
                        }
                        else{
                            Toast.makeText(RegistrationScreen.this, "yyy co", Toast.LENGTH_LONG).show();}


                    }

                    @Override
                    public void onFailure(Call<StringResponse> call, Throwable t) {

                        Toast.makeText(RegistrationScreen.this, call.toString(), Toast.LENGTH_SHORT).show();
                        Log.d("SERWER RESPONSE: ", "SA", t);


                    }
                });

    }


    // Funkcja sprawdzająca czy został wpisany email, hasło i potwierdzenie hasła
    public boolean checkLoginPasswordDouble(EditText remail_check, EditText rpassword_check,EditText rpassword_confirmation_check) {

        boolean corr1, corr2,corr3;

        // Sprawdź czy puste
        boolean email = remail_check.getText().toString().isEmpty();
        boolean password = rpassword_check.getText().toString().isEmpty();
        boolean password_confirmation =rpassword_confirmation_check.getText().toString().isEmpty();

        TextInputLayout email_check_parent = findViewById(R.id.remail_parent);
        TextInputLayout password_check_parent = findViewById(R.id.rpassword_parent);
        TextInputLayout password_confirmation_check_parent =findViewById(R.id.rpassword_conf_parent);

        // Weryfikacja loginu
        if (email) {
            email_check_parent.setError(getString(R.string.remail_cant_be_empty));
            corr1 = false;
        } else corr1 = true;

        // Weryfikacja hasła
        if (password) {
            password_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr2 = false;
        } else corr2 = true;

        // Weryfikacja potwierdzenia hasła
        if (password_confirmation) {
            password_confirmation_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr3 = false;
        } else corr3 = true;

        if(corr1 && corr2 && corr3) {

            EditText password_check = findViewById(R.id.rpassword_here);
            EditText password_confirmation_check = findViewById(R.id.rpassword_conf_here);
            String a = password_confirmation_check.getText().toString();
            String s = password_check.getText().toString();
            if(s.equals(a)) {
                registerSuccess.set(true);
                Toast.makeText(RegistrationScreen.this, "dobrze", Toast.LENGTH_SHORT).show();
            }else
                Toast.makeText(RegistrationScreen.this, "Hasła nie są tekie same", Toast.LENGTH_SHORT).show();
                registerSuccess.set(false);

        } else{ registerSuccess.set(false);
            Toast.makeText(RegistrationScreen.this, "zle", Toast.LENGTH_SHORT).show();
                }

        return registerSuccess.get();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}


