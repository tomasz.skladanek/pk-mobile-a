package pl.wat.dogweb;

import static androidx.core.content.ContextCompat.startActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

import pl.wat.dogweb.models.SharedPrefsDefaults;


public class AdminPanelScreen extends AppCompatActivity {


    public static final String TOKEN = SharedPrefsDefaults.TOKEN;

    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_panel);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

    }
    public void ToAddress(View view) {
        startActivity(new Intent(this, ShelterAddressScreen.class));
    }
    public void ToContact(View view){

        startActivity(new Intent(this,ShelterContactScreen.class));
    }
    public void ToDescription(View view){
        startActivity(new Intent(this,ShelterDescriptionScreen.class));
    }
    public void ToRole(View view){
        startActivity(new Intent(this,RoleChangeScreen.class));
    }
}
