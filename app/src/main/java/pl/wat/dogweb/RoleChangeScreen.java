package pl.wat.dogweb;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import pl.wat.dogweb.models.SharedPrefsDefaults;
import pl.wat.dogweb.models.StringResponse;
import pl.wat.dogweb.models.UpdateAddressRequest;
import pl.wat.dogweb.models.UpdateRoleRequest;
import pl.wat.dogweb.service.RetrofitClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoleChangeScreen extends AppCompatActivity {

    AtomicBoolean RoleSuccess = new AtomicBoolean(false);


    public static final String TOKEN = SharedPrefsDefaults.TOKEN;

    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_role_change);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

    }

    public void UpdateRole(View view) {

        EditText email_check = findViewById(R.id.role_change_email);
        EditText role_check = findViewById(R.id.role_change_role);


        if (checkIfEmptyRole(email_check, role_check)) {

            String email_txt = email_check.getText().toString();
            String role_txt = role_check.getText().toString();



            UpdateRoleRequest updateRoleRequest = new UpdateRoleRequest(
                    email_txt,
                    role_txt
                    );

            new RetrofitClass().getService().requestUpdateRole(PreferenceManager.getDefaultSharedPreferences(this).getString(SharedPrefsDefaults.TOKEN, null), updateRoleRequest).enqueue(new Callback<StringResponse>() {
                @Override
                public void onResponse(Call<StringResponse> call, Response<StringResponse> response) {
                    if (response.body().getResp().equals("Zmieniłeś role użytkownika")) {
                        Toast.makeText(RoleChangeScreen.this, response.body().getResp(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(RoleChangeScreen.this, response.body().getResp(), Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<StringResponse> call, Throwable t) {

                    Toast.makeText(RoleChangeScreen.this, call.toString(), Toast.LENGTH_SHORT).show();
                    Log.d("SERWER RESPONSE: ", "Error", t);


                }
            });
        }
    }




    // Funkcja sprawdzająca czy został wpisany email, hasło i potwierdzenie hasła
    public boolean checkIfEmptyRole(EditText email_check,
                                    EditText role_check  ) {

        boolean corr1, corr2;

        // Sprawdź czy puste
        boolean email = email_check.getText().toString().isEmpty();
        boolean role = role_check.getText().toString().isEmpty();

        TextInputLayout email_check_parent = findViewById(R.id.role_change_email_parent);
        TextInputLayout role_check_parent = findViewById(R.id.role_change_role_parent);


        // Weryfikacja emailu
        if (email) {
            email_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr1 = false;
        } else corr1 = true;

        // Weryfikacja Roli
        if (role) {
            role_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr2 = false;
        } else corr2 = true;


        if (corr1 && corr2 ) {
            RoleSuccess.set(true);
        } else RoleSuccess.set(false);

        return RoleSuccess.get();
    }

}