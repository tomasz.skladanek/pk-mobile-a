package pl.wat.dogweb;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pl.wat.dogweb.adapters.DogListAdapter;
import pl.wat.dogweb.adapters.ImageSliderAdapter;
import pl.wat.dogweb.models.Dog;
import pl.wat.dogweb.models.GetDogByIdRequest;
import pl.wat.dogweb.models.GetDogByIdResponse;
import pl.wat.dogweb.models.SharedPrefsDefaults;
import pl.wat.dogweb.models.Singletone;
import pl.wat.dogweb.models.StringResponse;
import pl.wat.dogweb.models.UpdateContactRequest;
import pl.wat.dogweb.service.RetrofitClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoScreen extends AppCompatActivity {


    private TextView dogName;
    private TextView dogDescription;

    private GetDogByIdResponse dog;
    private String dogFromId;

    private ArrayList<String> imageUrls = new ArrayList<>();

    // -1 jeśli nie jest ustawione id
    private Integer dogId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_screen);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dogId = getIntent().getIntExtra("dog-id", -1);

        dogName = findViewById(R.id.sample_dog_name);
        dogDescription = findViewById(R.id.textViewInfoDescription);

        int dogId = getIntent().getIntExtra("dog-id", -1);
        if (dogId == -1) {
            throw new IllegalStateException("DogId nie zostało ustawione!");
        } else {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            String token = sharedPreferences.getString(SharedPrefsDefaults.TOKEN, null);
            RetrofitClass.getService().getRandomDog(token).enqueue(new Callback<Dog>() {
                @Override
                public void onResponse(Call<Dog> call, Response<Dog> response) {
                    Dog dog = response.body();
                    // TODO - mam psa - działam na nim


                    dogName.setText(dog.getName(), TextView.BufferType.NORMAL);
                    dogDescription.setText(dog.getDescription(), TextView.BufferType.NORMAL);
                }

                @Override
                public void onFailure(Call<Dog> call, Throwable t) {
                    // TODO coś nie pykło
                }
            });
        }

        getImages();
            //pobieranie psa po id z serwera
//        GetDogByIdRequest getDogByIdRequest = new GetDogByIdRequest(dogFromId);
//        new RetrofitClass().getService().getDog(getDogByIdRequest).enqueue(new Callback<StringResponse>() {
//            @Override
//            public void onResponse(Call<StringResponse> call, Response<StringResponse> response) {
//                if (response.isSuccessful()) {
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<StringResponse> call, Throwable t) {
//                Toast.makeText(InfoScreen.this, "Nie udało się załadować ifnormacji o psie", Toast.LENGTH_SHORT).show();
//            }
//        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.info_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.fav_add_info:
                Toast.makeText(this, "Dodano do ulubionych!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, BigSlider.class));
                return true;
            case R.id.edit_info:
                Intent intent = new Intent(this, EditDogDetails.class);
                intent.putExtra("dog-id", dogId);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getImages() {

        imageUrls.add("https://schronisko.com/wp-content/uploads/2021/05/1.jpeg");
        imageUrls.add("https://schronisko.com/wp-content/uploads/2021/05/2.jpeg");
        imageUrls.add("https://schronisko.com/wp-content/uploads/2021/05/3.jpeg");
        imageUrls.add("https://schronisko.com/wp-content/uploads/2021/05/4.jpeg");
        imageUrls.add("https://schronisko.com/wp-content/uploads/2021/05/5.jpeg");
        imageUrls.add("https://schronisko.com/wp-content/uploads/2021/05/6.jpeg");

        initRecycleView();
    }

    private void initRecycleView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.recycler_view_slider);
        recyclerView.setLayoutManager(layoutManager);
        ImageSliderAdapter imageSliderAdapter = new ImageSliderAdapter(imageUrls, this);
        recyclerView.setAdapter(imageSliderAdapter);
    }
}
